package com.SimplonFilRouge.recyclaBack.Dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MaterialDto {
    private Integer id;
    private String name;
    private String image;
    private Set<String> matters;

}
