package com.SimplonFilRouge.recyclaBack.Controller;

import com.SimplonFilRouge.recyclaBack.Dto.MaterialDto;
import com.SimplonFilRouge.recyclaBack.Entity.Material;
import com.SimplonFilRouge.recyclaBack.Mapper.MaterialMapper;
import com.SimplonFilRouge.recyclaBack.Service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/materials")
public class MaterialController {

    @Autowired
    private MaterialService materialService;

    @Autowired
    private MaterialMapper materialMapper;

    @PostMapping
    public Material createMaterial(@RequestBody Material material) {
        return materialService.createMaterial(material);
    }

    @GetMapping
    public List<MaterialDto> getAllMaterials() {
        List<Material> materials = materialService.getAllMaterials();
        return materials.stream().map(materialMapper::toDto).collect(Collectors.toList());
    }

    @GetMapping("get/{id}")
    public ResponseEntity<MaterialDto> getMaterialById(@PathVariable(value = "id") Integer materialId) {
        Material material = materialService.getMaterialById(materialId);
        MaterialDto materialDto = materialMapper.toDto(material);
        return ResponseEntity.ok().body(materialDto);
    }

    @PutMapping("update/{id}")
    public ResponseEntity<Material> updateMaterial(@PathVariable(value = "id") Integer materialId, @RequestBody Material materialDetails) {
        Material updatedMaterial = materialService.updateMaterial(materialId, materialDetails);
        return ResponseEntity.ok(updatedMaterial);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> deleteMaterial(@PathVariable(value = "id") Integer materialId) {
        materialService.deleteMaterial(materialId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/recycling-score/{id}")
    public ResponseEntity<Integer> getRecyclingScore(@PathVariable(value = "id") Integer materialId) {
        Material material = materialService.getMaterialById(materialId);
        int score = materialService.calculateRecyclingScore(material);
        return ResponseEntity.ok().body(score);
    }
}
