package com.SimplonFilRouge.recyclaBack.Service;

import com.SimplonFilRouge.recyclaBack.Config.ResourceNotFoundException;
import com.SimplonFilRouge.recyclaBack.Entity.Material;
import com.SimplonFilRouge.recyclaBack.Entity.Matter;
import com.SimplonFilRouge.recyclaBack.Repository.MaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class MaterialService {

    @Autowired
    private MaterialRepository materialRepository;

    public Material createMaterial(Material material) {
        return materialRepository.save(material);
    }

    public List<Material> getAllMaterials() {
        return materialRepository.findAll();
    }

    public Material getMaterialById(Integer materialId) {
        return materialRepository.findById(materialId)
                .orElseThrow(() -> new ResourceNotFoundException("Material not found with id: " + materialId));
    }

    public Material updateMaterial(Integer materialId, Material materialDetails) {
        Material material = materialRepository.findById(materialId)
                .orElseThrow(() -> new ResourceNotFoundException("Material not found with id: " + materialId));

        material.setName(materialDetails.getName());
        material.setImage(materialDetails.getImage());
        material.setMatters(materialDetails.getMatters());

        return materialRepository.save(material);
    }

    public void deleteMaterial(Integer materialId) {
        Material material = materialRepository.findById(materialId)
                .orElseThrow(() -> new ResourceNotFoundException("Material not found with id: " + materialId));

        materialRepository.delete(material);
    }

    public int calculateRecyclingScore(Material material) {
        Set<Matter> matters = material.getMatters();
        int score = 0;

        for (Matter matter : matters) {
            switch (matter.getName().toUpperCase()) {
                case "METAL":
                    score += 90;
                    break;
                case "PAPER":
                    score += 70;
                    break;
                case "CARDBOARD":
                    score += 80;
                    break;
                case "CLOTH":
                    score += 60;
                    break;
                default:
                    score += 50;
                    break;
            }
        }
        if (matters.size() > 1) {
            score -= 10 * (matters.size() - 1);
        }
        return Math.max(0, Math.min(100, score));
    }
}
