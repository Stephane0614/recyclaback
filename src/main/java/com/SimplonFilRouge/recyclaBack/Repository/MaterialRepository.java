package com.SimplonFilRouge.recyclaBack.Repository;

import com.SimplonFilRouge.recyclaBack.Entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialRepository extends JpaRepository<Material, Integer> {
}

