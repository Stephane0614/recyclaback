package com.SimplonFilRouge.recyclaBack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecyclaBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecyclaBackApplication.class, args);
	}

}
