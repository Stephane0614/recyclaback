package com.SimplonFilRouge.recyclaBack.Mapper;

import com.SimplonFilRouge.recyclaBack.Dto.MaterialDto;
import com.SimplonFilRouge.recyclaBack.Entity.Material;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class MaterialMapper {

    public MaterialDto toDto(Material material) {
        MaterialDto dto = new MaterialDto();
        dto.setId(material.getId());
        dto.setName(material.getName());
        dto.setImage(material.getImage());
        dto.setMatters(material.getMatters().stream()
                .map(matter -> matter.getName())
                .collect(Collectors.toSet()));
        return dto;
    }
}
