package com.SimplonFilRouge.recyclaBack.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "matter")
public class Matter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @ManyToMany(mappedBy = "matters", fetch = FetchType.EAGER) // Change to EAGER loading for simplicity
    private Set<Material> materials = new LinkedHashSet<>();
}