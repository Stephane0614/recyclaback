package com.SimplonFilRouge.recyclaBack.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "materials")
public class Material {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "image", nullable = false, length = 100)
    private String image;

    @ManyToMany(fetch = FetchType.EAGER) // Change to EAGER loading for simplicity
    @JoinTable(name = "avoir",
            joinColumns = @JoinColumn(name = "id_materials"),
            inverseJoinColumns = @JoinColumn(name = "id"))
    private Set<Matter> matters = new LinkedHashSet<>();
}
