package com.SimplonFilRouge.recyclaBack;

import com.SimplonFilRouge.recyclaBack.Config.ResourceNotFoundException;
import com.SimplonFilRouge.recyclaBack.Entity.Material;
import com.SimplonFilRouge.recyclaBack.Entity.Matter;
import com.SimplonFilRouge.recyclaBack.Repository.MaterialRepository;
import com.SimplonFilRouge.recyclaBack.Service.MaterialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MaterialServiceTest {

    @InjectMocks
    private MaterialService materialService;

    @Mock
    private MaterialRepository materialRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCalculateRecyclingScore() {
        Material material = new Material();
        Set<Matter> matters = new HashSet<>();
        Matter metal = new Matter();
        metal.setName("METAL");
        Matter paper = new Matter();
        paper.setName("PAPER");
        matters.add(metal);
        matters.add(paper);
        material.setMatters(matters);

        int score = materialService.calculateRecyclingScore(material);
        assertEquals(100, score);
    }

    @Test
    void testGetMaterialById_NotFound() {
        when(materialRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> {
            materialService.getMaterialById(1);
        });
    }

    @Test
    void testCreateMaterial() {
        Material material = new Material();
        material.setName("Test Material");
        when(materialRepository.save(any(Material.class))).thenReturn(material);

        Material createdMaterial = materialService.createMaterial(material);
        assertEquals("Test Material", createdMaterial.getName());
    }
}
